Getting started
===============

The following is a guide for installation of Resummino on Linux, but a similar procedure has been tested to work on other UNIX-like systems like OS X.

Prerequisites
-------------

Resummino requires several packages, all of which are open source and publicly available. Resummino needs:

* Some `Boost <http://www.boost.org/>`_ headers.
* The `GNU Scientific Library <http://www.gnu.org/software/gsl>`_, including headers.
* The `LHAPDF library <http://lhapdf.hepforge.org/>`_, including headers.
* The `QCDLoop package <http://qcdloop.fnal.gov/>`_, which is included in Resummino itself for convenience.
* The `CMake <http://www.cmake.org>`_ program for compilation.

The first two can be installed in RHEL/Scientific Linux or Fedora by using::

    $ sudo yum install boost boost-devel gsl gsl-devel

In Debian/Ubuntu you can use a similar command for the same purpose::

    $ sudo apt-get install libboost-dev libgsl0-dev

You will need to install the LHAPDF library by following the instructions at `the website <http://lhapdf.hepforge.org/install>`_, if you have not done so yet. You should also write down the path where LHAPDF is installed to.

Most modern systems already include CMake. Otherwise you should install it by following the instructions on `the CMake website <http://www.cmake.org/cmake/help/install.html>`_.

Obtaining Resummino
-------------------

In general you will want to obtain the latest stable source tarball, from the `Resummino website <http://www.resummino.org>`_.

The current release is ``1.0.7`` and you can download it directly from `<https://bitbucket.org/resummino/resummino/downloads/resummino-1.0.7.tar.bz2>`_.

.. note::

    It is highly advised to stay up to date with the latest Resummino release, since older versions may contain known bugs. You can subscribe to the low-volume `resummino-anounce mailing list <https://groups.google.com/d/forum/resummino-announce>`_ to get notified when a new version appears.

Compilation
-----------

Once you have the source tarball, you first have to extract the contents, create a new folder to hold the binaries (out-of-source build), and compile it::

    $ tar -xvjf resummino-1.0.7.tar.bz2
    $ cd resummino-1.0.7
    $ mkdir build && cd build
    $ cmake .. [options]

At this point CMake will check that all dependencies are correctly installed and will create the appropriate Makefile for compilation. If the LHAPDF library is not installed in a directory referenced by ``LD_LIBRARY_PATH``, you will have to specify the directory with the ``-DLHAPDF=/path/to/lhapdf`` option. With this CMake will look for the libraries at ``/path/to/lhapdf/lib`` and for the headers at ``/path/to/lhapdf/include``. You can also specfy these two separately by using the ``-DLHAPDF_LIB_DIR=/path/to/lhapdf/lib`` and ``-DLHAPDF_INCLUDE_DIR=/path/to/lhapdf/include`` options.

You can also specify the path where you want Resummino installed by using the ``-DCMAKE_INSTALL_PREFIX=/path/to/install`` option.

After this, you can use the generated Makefile to build Resummino::

    $ make

Further options can be found in the `CMake documentation <http://cmake.org/cmake/help/v2.8.11/cmake.html>`_.

Installation
------------

Once compiled, installation is just a matter of issuing the command::

    $ make install

This will install Resummino in the previously specified folder or in the default installation directory. (You may need ``root`` permission, in which case you may use ``sudo``). Make sure that in either case that folder is in your ``PATH`` variable. For example, if you have installed Resummino to a non-standard folder ``/path/to/resummino`` (maybe because you do not have ``root`` permissions) and that folder is not already in your path, you may want to include something like the following in your ``~/.profile`` file (or ``~/.bash_profile`` if it exists)::

    export PATH=/path/to/resummino/bin:$PATH
