Computing with Resummino
========================

Input file
----------

The different input parameters that define the process and the type of computation to perform are passed to Resummino using an input file. An example input file is found in the release tarball as ``input/resummino.in``. The SUSY model is specified in a separate file using the `SLHA format <http://home.fnal.gov/~skands/slha/>`_. An example SLHA file can be found in the release tarball as ``input/slha.in``. The particular SLHA file to use is specified in the Resummino input file using the ``slha`` parameter. Please note that the path to the SLHA file must be specified relative to the Resummino input file, so in this case the variable will be ``slha = slha.in``.

Command-line options
--------------------

Often times you will want to compute a set of cross sections with similar but not exactly the same parameters. In that case, it would be impractical to have a huge set of similar input files, but you can override one or a few input file parameters using command-line options. One common example of this would be to run the same process with different PDF subsets to later compute the PDF uncertainties for the process. In those cases, instead of having one input file per PDF subset, you can just use a common input file and use the ``--pdfset_lo`` and ``--pdfset_nlo`` command-line options to override the PDF subsets.

If a command-line option is specified, the value from the input file is ignored.

Advanced input
--------------

If you need more advanced input options, you may specify the special ``-`` input file name, and Resummino will read the input file contents from ``stdin``. In that way, you can, e.g., use Unix pipes to write the input file on-the-fly.

Input reference
---------------

The following is the full list of options understood by Resummino, including the names as input file options and as command-line arguments. Except otherwise noted, all file options are mandatory, and all command-line options are optional.

============================= ===================================== ========================================================================================================
File option                   Command-line option                   Description
============================= ===================================== ========================================================================================================
``collider_type``             *None*                                Sets the hadron collider type. Can be either ``proton-proton`` or ``proton-antiproton``.
----------------------------- ------------------------------------- --------------------------------------------------------------------------------------------------------
``center_of_mass_energy``     *None*                                Sets the center of mass energy in GeV.
----------------------------- ------------------------------------- --------------------------------------------------------------------------------------------------------
``particle1``                 ``--particle1``                       Sets the first outgoing particle using the PDG numbering scheme. See `PDG numbering scheme`_.
----------------------------- ------------------------------------- --------------------------------------------------------------------------------------------------------
``particle2``                 ``--particle2``                       Sets the second outgoing particle using the PDG numbering scheme. See `PDG numbering scheme`_.
----------------------------- ------------------------------------- --------------------------------------------------------------------------------------------------------
``result``                    *None*                                Sets the process type. Can be ``total`` for total cross section, ``m`` to compute the invariant-mass
                                                                    distribution at the invariant mass set in the variable ``m``, ``pt`` to compute the transverse-momentum
                                                                    distribution at transverse momentum ``pt``.
----------------------------- ------------------------------------- --------------------------------------------------------------------------------------------------------
``m``                         ``--invariant-mass``, ``-m``          Sets the invariant mass. ``auto`` sets it to :math:`\sqrt{(p_1 + p_2)^2}`
----------------------------- ------------------------------------- --------------------------------------------------------------------------------------------------------
``pt``                        ``--transverse-momentum``, ``-t``     Sets the transverse momentum. ``auto`` integrates over all the possible values.
----------------------------- ------------------------------------- --------------------------------------------------------------------------------------------------------
``slha``                      *None*                                Sets the SLHA file that defines the SUSY model, relative to the location of the input file.
----------------------------- ------------------------------------- --------------------------------------------------------------------------------------------------------
``pdf_format``                *None*                                Sets the LHAPDF file format. Can be ``lhgrid`` or ``lhpdf``. If not present or empty, the default
                                                                    ``lhgrid`` will be used.
----------------------------- ------------------------------------- --------------------------------------------------------------------------------------------------------
``pdf_lo``, ``pdf_nlo``       *None*                                Set the PDF set to use, defined by the LHAPDF name, e.g., ``MSTW2008lo68cl`` or ``MSTW2008nlo68cl``.         
----------------------------- ------------------------------------- --------------------------------------------------------------------------------------------------------
``pdfset_lo``, ``pdfset_nlo`` ``--pdfset_lo``, ``--pdfset_nlo``     Set the PDF subset to use, defined as a number. Usually the central PDF subset is ``0``.
----------------------------- ------------------------------------- --------------------------------------------------------------------------------------------------------
``mu_f``, ``mu_r``            ``--mu_f``, ``--mu_r``                Set the factorization and normalization scale factors. These will be multiplied by the sum of the masses
                                                                    of the outgoing particles to obtain the corresponding scales.
----------------------------- ------------------------------------- --------------------------------------------------------------------------------------------------------
*None*                        ``--output_file``, ``-o``             Sets the name of the file to output the results in JSON format.
----------------------------- ------------------------------------- --------------------------------------------------------------------------------------------------------
*None*                        ``--lo``, ``-nlo``                    Stop the computations after LO or NLO results are obtained. The rest of the cross sections will be
                                                                    printed as being 0 in the proper units.
============================= ===================================== ========================================================================================================

PDG numbering scheme
--------------------

For reference we include below the PDG codes for the relevant particles. Particles and antiparticles have opposite signs.

Sleptons
~~~~~~~~
* :math:`\tilde e_{\rm L}^-`: ``1000011``
* :math:`\tilde e_{\rm R}^-`: ``2000011``
* :math:`\tilde \nu_{e_{\rm L}}`: ``1000012``
* :math:`\tilde \mu_{\rm L}^-`: ``1000013``
* :math:`\tilde \mu_{\rm R}^-`: ``2000013``
* :math:`\tilde \nu_{\mu_{\rm L}}`: ``1000014``
* :math:`\tilde \tau_1^-`: ``1000015``
* :math:`\tilde \tau_2^-`: ``2000015``
* :math:`\tilde \nu_\tau`: ``1000016``

Gauginos
~~~~~~~~

* :math:`\tilde \chi_1^0`: ``1000022``
* :math:`\tilde \chi_2^0`: ``1000023``
* :math:`\tilde \chi_3^0`: ``1000025``
* :math:`\tilde \chi_4^0`: ``1000035``
* :math:`\tilde \chi_1^+`: ``1000024``
* :math:`\tilde \chi_2^+`: ``1000037``

