Introduction
============

Welcome to the Resummino documentation.

Resummino computes resummation predictions for Beyond the Standard Model (BSM) particle production at hadron colliders up to the NLO+NLL level. Currently the processes implemented include gaugino-pair production and slepton-pair production. It is able to compute total cross sections as well as invariant-mass and transverse-momentum distributions.

Physics of Resummino
~~~~~~~~~~~~~~~~~~~~

This manual explains how to obtain, install, and use Resummino, documenting its features and options. If you want to understand the physics behind Resummino, you will want to read the physics manual, published as

* B\. Fuks, M. Klasen, D. R. Lamprea, and M. Rothering, Eur. Phys. J. C **73**, 2480 (2013), `arXiv:1304.0790 [hep-ph] <http://arxiv.org/abs/1304.0790>`_.

How to cite Resummino
~~~~~~~~~~~~~~~~~~~~~

If you are using Resummino for slepton pair production, we ask you to please cite:

* G. Bozzi, B. Fuks, and M. Klasen, Phys. Rev. D **74**, 015001 (2006); Nucl. Phys. B **777**, 157 (2007); Nucl. Phys. B **794**, 46 (2007).
* B. Fuks, M. Klasen, D. R. Lamprea, and M. Rothering, Eur. Phys. J. C **73**, 2480 (2013).

If you use it for gaugino pair production, please cite:

* J. Debove, B. Fuks, and M. Klasen, Phys. Lett. B **688**, 208 (2010); Nucl. Phys. B **842**, 51 (2011); Nucl. Phys. B **849**, 64 (2011).
* B. Fuks, M. Klasen, D. R. Lamprea, and M. Rothering, JHEP **1210**, 081 (2012); Eur. Phys. J. C **73**, 2480 (2013).

Authors and license
~~~~~~~~~~~~~~~~~~~

Resummino is open source software under the terms of the `European Union Public Licence (EUPL), version 1.1 or later <http://opensource.org/licenses/EUPL-1.1>`_, and is actively developed at the Institut für Theoretische Physik, Universität Münster, Germany, by David Lamprea and Marcel Rothering at the Research Group of Prof. Dr. Michael Klasen.

For more information you can visit the `Resummino website <http://www.resummino.org>`_.
