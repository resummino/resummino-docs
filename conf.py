import sys, os

extensions = ['sphinx.ext.mathjax']
templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'index'
project = u'Resummino'
copyright = u'2013, 2014 David R. Lamprea'
version = '1.0'
release = '1.0'
exclude_patterns = ['_build']
pygments_style = 'sphinx'
html_theme = 'default'
html_static_path = ['_static']
html_domain_indices = False
html_use_index = False
html_show_sourcelink = False

htmlhelp_basename = 'ResumminoDoc'

latex_elements = {
    'papersize': 'a4paper',
    }
latex_documents = [
    ('index', 'Resummino.tex', u'Resummino Documentation',
     u'David R. Lamprea', 'manual'),
    ]

man_pages = [
    ('index', 'resummino', u'Resummino Documentation',
     [u'David R. Lamprea'], 1)
    ]

texinfo_documents = [
    ('index', 'Resummino', u'Resummino Documentation',
     u'David R. Lamprea', 'Resummino', 'One line description of project.',
     'Miscellaneous'),
    ]
