Analyzing the output
====================

Resummino output format
-----------------------

Each time Resummino is called, it produces as output three cross sections (the cross section for the specified process at LO, NLO and NLO matched with NLL). This can be either a total cross section for the process or a point in a transverse momentum or invariant mass differential distribution. These numbers (including the appropriate units) will be printed to ``stdout`` in a human-readable form.

However, usually a subsequent analysis of this data is required to, e.g., determine the uncertainties, or to obtain a distribution plot, which is simplified by a computer-friendly output format. While there are established standards for the input to Resummino (SLHA and LHAPDF), there is so far no clearly defined way to output cross sections in a standarized way. The decision has been to include an option to output the results to a `JSON <http://www.json.org/>`_ file. This is a well-known and established format with implementations in virtually every programming language. To obtain the JSON output, you have to specify the ``--output_file`` command-line argument.

A typical JSON output file from Resummino looks like this::

    {
        "key": "",
        "pt": -1,
        "m": -1,
        "pdflo": "MSTW2008lo68cl",
        "pdfsetlo": 0,
        "pdfnlo": "MSTW2008nlo68cl",
        "pdfsetnlo": 0,
        "muf": 0.5,
        "mur": 0.5,
        "lo": 2.8873845e-03,
        "nlo": 0.0000000e+00,
        "nll": 0.0000000e+00,
        "nllj": 0.0000000e+00,
        "units": "pb"
    }

Most of the field names should be self-explanatory. The ``key`` parameter is currently not used. A value of ``-1`` for ``pt`` (``m``) means that the transverse momentum (invariant mass) has been set to ``auto``.

Example analysis script
-----------------------

As an example we will use a Python analysis script which can handle the most basic cases for computations: total cross sections, invariant mass and transverse momentum distribution with PDF and scale uncertainties at LO, NLO and NLO+NLL. This script can be found at `https://bitbucket.org/resummino/resummino-analysis <https://bitbucket.org/resummino/resummino-analysis>`_.

For example, we can compute a transverse momentum distribution including scale variation::

    $ for pt in $(seq 5 5 100); do
    $ for $scale in 0.5 1 2; do
    $ resummino --pt=$pt --mu_f=$mu --mur_r=$mu --output_file=output_pt${pt}_scale${mu}.out
    $ done
    $ done

In a real-life case you will want to compute all those processes in parallel (probably in a cluster). That is why those loops are not included in Resummino itself.

Once you have computed all those processes you may create a table that includes the computed transverse momentum distribution with scale uncertainties::

    $ resummino_analysis *.out > results.table

That table can be plotted, e.g., by using ``gnuplot``::

    $ gnuplot
    > plot 'results.table' u 1:10:11:12 w yerrbars  # NLO pt distribution with mu_f uncertainty
